import React from 'react'
import Button from '../../../node_modules/@material-ui/core/Button';
import Dialog from '../../../node_modules/@material-ui/core/Dialog';
import DialogActions from '../../../node_modules/@material-ui/core/DialogActions';
import DialogContent from '../../../node_modules/@material-ui/core/DialogContent';
import DialogTitle from '../../../node_modules/@material-ui/core/DialogTitle';

import './FormDialog.scss'

class FormDialog extends React.Component{
    state = {
        description: '',
        open: true ,
    };

    render () {
        const { closeModal } = this.props;
        return (
            <div>
                <Dialog
                    open={this.state.open}
                    // onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title"> Workout Plan:</DialogTitle>
                    <DialogContent>
                        <textarea
                            autoFocus
                            id="name"
                            cols="30"
                            rows="10"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={ closeModal } color="primary">
                            Cancel
                        </Button>
                        <Button onClick={ closeModal } color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default FormDialog;