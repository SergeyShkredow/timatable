import React, { Component } from 'react';
import UUID from 'uuid/v4';

import FormDialog from "../FormDialog/formDialog";
import TitleWeek from "../TitleWeek/titleWeek";
import DataList from '../DataList/dataList'
import './Timetable.css';

export default class Timetable extends Component {

    state = {
        formDialogOpen: false,
        isVisible : false
    };

    _openFormDialog() {
        this.setState({formDialogOpen: true});
    }

    _closeFormDialog() {
        this.setState({formDialogOpen: false});
    }
    movePrevious () {
    }

    moveNext () {
        console.log('OK');
    }

   render () {
        const { formDialogOpen } = this.state;
        const oneWeekDataList = [
            {id: UUID(), title: 'Sunday', dayWeek: '30'},
            {id: UUID(), title: 'Monday', dayWeek: '31'},
            {id: UUID(), title: 'Tuesday', dayWeek: '1'},
            {id: UUID(), title: 'Wednesday', dayWeek: '2'},
            {id: UUID(), title: 'Thursday', dayWeek: '3'},
            {id: UUID(), title: 'Friday', dayWeek: '4'},
            {id: UUID(), title: 'Saturday', dayWeek: '5'},
         ];
       console.log(oneWeekDataList)
        return (
            <div className="listWeek">
                <div className="listWeek__header">
                    <h3 className="">
                        <p> week</p>
                    </h3>
                    <div className="listWeek__header--buttons">
                        <button onClick={ this.movePrevious.bind(this) } className="listWeek__header--buttons-btnPrev" id="">&#60;
                            </button>
                        <button  onClick={ this.moveNext.bind(this) } className="listWeek__header--buttons-btnNext" id="">&#62;
                            </button>
                    </div>
                </div>
                <div className="">
                    <DataList dataItems = {oneWeekDataList} onClick={this._openFormDialog.bind(this)}/>
                </div>
                    {formDialogOpen ? <FormDialog closeModal={this._closeFormDialog.bind(this)} />
                        : ''}
             </div>

        )
    }
}
