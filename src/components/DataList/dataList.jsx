import React from 'react';
import { inject, observer, PropTypes as MobxPropTypes } from 'mobx-react';
import PropTypes from 'prop-types';
import './DataList.css'
import DataListItem from './DataListItem'

const DataList = ({ dataItems }) => (
    <ul className="listWeek__content">
        {dataItems.map(dataItem => (
            <DataListItem
                key={dataItem.id}
                title={dataItem.title}
                dayWeek={dataItem.dayWeek}
                addWorkout = {dataItem.addWorkout}
                // initialData = {dataItem.initialData}
            />
        ))}
    </ul>
);

DataList.propTypes = {
    dataItems: MobxPropTypes.arrayOrObservableArrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        dayWeek: PropTypes.string.isRequired,
        // initialData: PropTypes.object,
        addWorkout: PropTypes.func.isRequired
    })).isRequired,
};

export default DataList;

// export default inject(({ dataItemsStore: { dataItems } }) => ({
//     dataItems,
// }))(observer(DataList));
