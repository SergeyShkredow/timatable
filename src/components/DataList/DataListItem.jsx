import React from 'react'
import PropTypes from 'prop-types'

import '../ItemDay/itemDay.css'

const DataListItem = ({
    id,
    title,
    dayWeek,
    addWorkout = () => {
        console.log("addWorkout")
},
    }) => {
    return (
        <li className="Data-listItem" onClick={addWorkout}>
            <strong className="Data-listItemTitle">{title}</strong>
            <p className="">{dayWeek}</p>
        </li>
    );
};

DataListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    dayWeek: PropTypes.string.isRequired,
    // initialData: PropTypes.object,
    addWorkout: PropTypes.func.isRequired,
};

export default DataListItem;