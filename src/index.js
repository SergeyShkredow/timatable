import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import Timetable from './components/Timetable/index';
import DataList from './components/DataList/dataList'
import PropTypes from "prop-types";

import './index.css';
import DataStore from './components/stores/DataStore';

// const fakeDataList = [];
//
// for (let i = 0; i <= 21; i++) {
//     fakeDataList.push({
//         id: 'data-number-{i}',
//         title: 'data-day-{i}',
//         initialData: {},
//     })
// }

// const store = new DataStore(fakeDataList);

const MyApp  = () => {

       {/*<Provider store={store}>*/}
       return (
        <div className="myApp">
            <div className="myApp-formContainer">
                <Timetable />
            </div>
        </div>
       )
        // </Provider>
    };

ReactDOM.render(<MyApp />, document.getElementById('root'));

